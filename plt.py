import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import sys
style.use('fivethirtyeight')

fig = plt.figure()

ax1 = fig.add_subplot(1,1,1)
filename = sys.argv[1]
def animate(i):
    graph_data = open(str(filename),'r').read()
    lines = graph_data.split('\n')
    xs = []
    ys = []
    zs = []
    for line in lines:
        if len(line) > 1:
            x, y, z = line.split(',', 2)
            xs.append(int(x))
            ys.append(int(y))
            zs.append(int(z))
    ax1.clear()
    ax1.plot(xs, label="Darek1")
    ax1.plot(ys, label="Darek2")
    ax1.plot(zs, label="Darek3")
ani = animation.FuncAnimation(fig, animate, interval=1000)
plt.xticks([])
plt.show()
